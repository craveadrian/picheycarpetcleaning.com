<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft">
						<a href="home"><img id="banner_img" src="public/images/common/mainLogo.png" alt="Pichey Carpet Cleaners"></a>
				</div>
				<div class="hdRight">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
							<li <?php $this->helpers->isActiveMenu("tile-crout-cleaning"); ?>><a href="<?php echo URL ?>home#tile-grout">TILE &amp; GROUT CLEANING</a></li>
							<!-- <li <?php //$this->helpers->isActiveMenu("gallery"); ?>><a href="<?php //echo URL ?>gallery#content">GALLERY</a></li> -->
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<h5>OUR REPUTATION IS SPOTLESS</h5>
				<img src="public/images/common/bg2.jpg" alt="Image Background" class="image-banner">
				<div class="panel_1">
					<p>Pichey Carpet and Upholstery Cleaning<span>in Business Since 1960</span></p>
				</div>
				<div class="panel_2">
					<h5>Serving Lansing & Flint plus Surrounding Areas</h5>
					<div class="left">
						<p><img src="public/images/common/emailIcon.png" alt="Email Icon"> VISIT US <span><?php $this->info(["email","mailto"]); ?></span></p>
					</div>
					<div class="right">
					<p><img id="banner_phone_icon" src="public/images/common/phoneIcon.png" alt="Phone Icon"> PHONE
						<span class="phone"><?php $this->info(["phone","tel"]); ?> <br />
						<?php $this->info(["phone_2","tel"]); ?><br />
						<?php $this->info(["phone_3","tel"]); ?><br />
					</span></p>
					</div>
				</div>
				<div class="bar">
					<p><a href="contact#content">FREE ESTIMATE</a></p>
				</div>
			</div>
		</div>
	<?php //endif; ?>
