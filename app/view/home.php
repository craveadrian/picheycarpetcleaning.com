<div id="content">
	<div id="welcome">
		<div class="row">
			<!-- <img src="public/images/content/welcome-img.jpg" alt="profile"> -->
			<img src="public/images/common/20180705_110647_resized.jpg" alt="profile">

			<div class="container">
				<h3>WELCOME</h3>
				<h1>Pichey Carpet Cleaners</h1>
				<p>At Pichey Carpet Cleaners, we guarantee satisfaction on all of the work we perform at your home or business. Our distinction is the quality of service we bring to our customers. Accurate knowledge of our trade combined with ability is what makes us true professionals. Above all our customer satisfaction is always our goal.</p>
				<a href="about#content" class="btn">LEARN MORE</a>
			</div>
			<div class="redbox"></div>
			<p>“Don’t Settle for the Rest, Get the Best” </p>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="whyChooseUs">
		<div class="row">
			<div class="panel">
				<div class="lane fl"></div>
				<div class="left fl">
					<h2>WHY CHOOSE US</h2>
					<h3>Residential & Commercial</h3>
					<h4>Our Complete Cleaning Process Includes:</h4>
						<ul class="list-col2">
							<li><p>Self Contained Mobile Unit</p></li>
							<li><p>We Do Not Use Your Hot Water Or Electricity, & The Machine Noise Stays Out In The Truck.</p></li>
							<li><p>No Shrinking & No Soaking</p></li>
							<li><p>Furniture Cleaning</p></li>
							<li><p>Auto Rug & Upholstery</p></li>
							<li><p>Pre-spotting</p></li>
							<li><p>Stain Removal Including Red Food Dyes</p></li>
							<li><p>Furniture Moving</p></li>
							<li><p>Animal Odors Removed</p></li>
							<li><p>Kills Infectious Germs</p></li>
							<li><p>Carpet Protectors</p></li>
							<li><p>Fast Drying</p></li>
						</ul>
						<p>Specialists in Spotting Techniques</p>
				</div>
				<div class="right fl">
					<!-- <img src="public/images/content/img1.jpg" alt="WHY CHOOSE US IMAGE"> -->
					<img id="welcome_img" src="public/images/content/welcome-img.jpg" alt="WHY CHOOSE US IMAGE">
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div id="ourRecentWork">
		<div class="row">
			<div class="top">
				<div class="topLeft fl">
					<h2>OUR RECENT WORK</h2>
				</div>
				<div class="topRight fr">
					<p>Instead of settling for just any cleaning company, you can count on the professionals with 60 years of experience to get the job done right.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom">
				<div class="box">
					<ul>
						<li>Equipped with the latest tools for Tile & Groot cleaning</li>
						<li>Kitchen Carpet Specialist</li>
						<li>Specialist in Spotting techniques </li>
						<li>Soil is extracted not scrubbed deeper into the fabric</li>
						<li>We use environmentally safe products</li>
					</ul>
				</div>
				<!-- <a href="<?php echo URL ?>gallery#content"> --><div class="box overlay"><img src="public/images/content/1.jpg" alt="Images 1"></div><!-- </a> -->
				<!-- <a href="<?php echo URL ?>gallery#content"> --><div class="box overlay"><img src="public/images/content/2.jpg" alt="Images 1"></div><!-- </a> -->
				<!-- <a href="<?php echo URL ?>gallery#content"> --><div class="box overlay"><img src="public/images/content/3.jpg" alt="Images 1"></div><!-- </a> -->
				<!-- <a href="<?php echo URL ?>gallery#content"> --><div class="box overlay"><img src="public/images/content/4.jpg" alt="Images 1"></div><!-- </a> -->
				<!-- <a href="<?php echo URL ?>gallery#content"> --><div class="box big overlay"><img src="public/images/content/5.jpg" alt="Images 1"></div><!-- </a> -->
				<!-- <a href="<?php echo URL ?>gallery#content"> --><div class="box overlay"><img src="public/images/content/6.jpg" alt="Images 1"></div><!-- </a> -->
			</div>
		</div>
	</div>


	<div id="tile-grout">
	  <div class="row">
	    <h2>TILE & GROUT CLEANING</h2>
		<p>We use the most advanced grout cleaning machine in the business. This machine uses high water pressure with the best cleaning products
	       available. It scribs the floor, then vacuums up the dirty water that other machines leave behind all in one operation. Our vacuum system is
	       powered by a 24 horse power engine.
	    </p>

	    <!-- <a href="public/docs/coupon-4.pdf" target="_blank" class="coupon">
					<img class="img-responsive" src="public/images/content/coupon-4.jpg">
			</a> -->

			<a href="public/docs/new_coupon_4.pdf" target="_blank" class="coupon">
					<img class="img-responsive" src="public/images/content/new_coupon_4.png">
			</a>

	  </div>
	</div>



	<div id="testimonials">
		<div class="row">
			<div class="panel">
				<img src="public/images/common/mainLogo.png" alt="Pichey Carpet Cleaners Main Logo">
				<h2>TESTIMONIALS</h2>
				<p>“I am so thankfulfor Pichey Cleaning. We have a baby that is crawling; and, I was concerned about the carpet not being clean enough for little Daisy. It sure is clean now!” <span><strong class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</strong> - Bailey</span></p>
				<p>“My carpet looks like new. I will be calling Pichey’s for all my future cleanings.” <span><strong class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</strong> - Hillary</span></p>
				<p>“I just moved into my apartment and my carpet it sure needed cleaning. Thanks to Pichey’s, it looks great” <span><strong class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</strong> - Alex</span></p>
			</div>
		</div>
	</div>
	<div id="contact">
		<div class="row">
			<h2>CONTACT US</h2>
			<h3>Free Estimates</h3>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email Address">
				</label>
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
				</label>
				<label><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
			</form>
		</div>
	</div>
	<div id="contactDetails">
		<div class="row">
			<div class="left fl">
				<img src="public/images/content/mail.png" alt="Email Icon">
				<h5>VISIT US</h5>
				<p><?php $this->info(["email","mailto"]); ?></p>
			</div>
			<div class="mid fl">
				<img src="public/images/content/phone.png" alt="Phone Icon">
				<h5>PHONE</h5>
				<p> <span><?php $this->info(["phone","tel"]); ?></span><span><?php $this->info(["phone_2","tel"]); ?></span><span> <?php $this->info(["phone_3","tel"]); ?></span></p>
			</div>
			<div class="right fr">
				<img src="public/images/content/location.png" alt="Location Icon">
				<h5>LOCATION</h5>
				<a href="https://www.google.com.ph/maps?q=Lansing,+MI&um=1&ie=UTF-8&sa=X&ved=0ahUKEwi5-PT_6tLbAhXLfbwKHXDABkoQ_AUICigB"><p>Lansing, MI</p></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
