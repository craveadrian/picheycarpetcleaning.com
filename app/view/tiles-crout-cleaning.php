<div id="content">
  <div class="row">
    <h1>TILE & GROUT CLEANING</h1>
    <a href="public/docs/coupon-4.pdf" target="_blank" class="coupon">
		<img class="img-responsive" src="public/images/content/coupon-4.jpg">
	</a>
	<p>We use the most advanced grout cleaning machine in the business. This machine uses high water pressure with the best cleaning products
       available. It scribs the floor, then vacuums up the dirty water that other machines leave behind all in one operation. Our vacuum system is 
       powered by a 24 horse power engine.
    </p>
  </div>
</div>
