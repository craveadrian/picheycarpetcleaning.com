<div id="content">
  <div class="row">
    <h1>SERVICES</h1>
    <br>
    <h2>Specialists in Spotting Techniques</h2>
    <h3>Residential &amp; Commercial</h3>
    <br>
	<h4>Our Complete Cleaning Process Includes:</h4>
		<ul>
			<li><p>Self Contained Mobile Unit</p></li>
			<li><p>We Do Not Use Your Hot Water Or Electricity, & The Machine Noise Stays Out In The Truck.</p></li>
			<li><p>No Shrinking & No Soaking</p></li>
			<li><p>Furniture Cleaning</p></li>
			<li><p>Auto Rug & Upholstery</p></li>
			<li><p>Pre-spotting</p></li>
			<li><p>Stain Removal Including Red Food Dyes</p></li>
			<li><p>Furniture Moving</p></li>
			<li><p>Animal Odors Removed</p></li>
			<li><p>Kills Infectious Germs</p></li>
			<li><p>Carpet Protectors</p></li>
			<li><p>Fast Drying</p></li>
		</ul>

		
		<a href="public/docs/coupon-2.pdf" target="_blank" class="coupon">
			<img class="img-responsive" src="public/images/content/coupon-2.jpg">
		</a>
  </div>
</div>
