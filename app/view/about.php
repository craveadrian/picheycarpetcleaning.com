<div id="content">
  <div class="row">
    <h1>ABOUT US</h1>
    <p>At Pichey Carpet Cleaners, we guarantee satisfaction on all of the work we perform at your home or business. Our distinction
       is the quality of service we bring to our customers. Accurate knowledge of our trade combined with ability is what makes us 
       true professionals. Above all our customer satisfaction is always our goal.
    </p>
    <div class="about-map-container">
    	<h5>Serving Lansing &amp; Flint plus Surrounding Areas</h5>
    	<img src="public/images/content/map.png">
   	</div>
  </div>
</div>
